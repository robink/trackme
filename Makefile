# Copyright (C) Robin Krahl <robin.krahl@ireas.org>
# SPDX-License-Identifier: CC0-1.0

.PHONY: all
all: lint build

.PHONY: build
build:
	cargo build --all-targets --all-features

.PHONY: check
check:
	cargo check --all-targets --no-default-features
	cargo check --all-targets --no-default-features --features intervals_icu
	cargo check --all-targets --no-default-features --features strava
	cargo check --all-targets --all-features

.PHONY: lint
lint: check
	cargo fmt -- --check
	cargo clippy --all-targets
	cargo doc --no-deps --all-features
	reuse lint

.PHONY: ci
ci: export RUSTFLAGS=-D warnings
ci: export RUSTDOCFLAGS=-D warnings
ci: check lint build
