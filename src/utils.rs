// Copyright (C) Robin Krahl <robin.krahl@ireas.org>
// SPDX-License-Identifier: GPL-3.0-or-later

use xdg::BaseDirectories;

pub fn base_directories() -> Option<BaseDirectories> {
    BaseDirectories::with_prefix("trackme").ok()
}

#[cfg(feature = "ureq")]
pub fn agent() -> ureq::Agent {
    ureq::AgentBuilder::new()
        .user_agent(concat!("trackme/", env!("CARGO_PKG_VERSION")))
        .build()
}
