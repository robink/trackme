// Copyright (C) Robin Krahl <robin.krahl@ireas.org>
// SPDX-License-Identifier: GPL-3.0-or-later

#[cfg(feature = "intervals_icu")]
mod intervals_icu;
#[cfg(feature = "strava")]
mod strava;

use std::path::Path;

use anyhow::Result;
use clap::{Args, Subcommand, ValueEnum};
use merge::Merge;
use serde::Deserialize;

use crate::commands::{Exec, Global};

pub trait Platform: Send + Sync {
    fn id(&self) -> &'static str;
    fn upload(&self, path: &Path, name: Option<&str>) -> Result<String>;
}

#[derive(Debug, Default, Args, Deserialize, Merge)]
pub struct PlatformArgs {
    #[cfg(feature = "intervals_icu")]
    #[clap(flatten)]
    #[serde(default)]
    intervals_icu: intervals_icu::IntervalsIcu,

    #[cfg(feature = "strava")]
    #[clap(flatten)]
    #[serde(default)]
    strava: strava::Strava,
}

impl PlatformArgs {
    pub fn platform(&self, platform: PlatformSpec) -> &dyn Platform {
        match platform {
            #[cfg(feature = "intervals_icu")]
            PlatformSpec::IntervalsIcu => &self.intervals_icu,
            #[cfg(feature = "strava")]
            PlatformSpec::Strava => &self.strava,
        }
    }

    pub fn platforms(&self, mut platforms: &[PlatformSpec]) -> Vec<&dyn Platform> {
        if platforms.is_empty() {
            platforms = PlatformSpec::value_variants();
        }
        platforms
            .iter()
            .map(|platform| self.platform(*platform))
            .collect()
    }
}

#[derive(Debug, Subcommand)]
pub enum PlatformCommand {
    #[cfg(feature = "strava")]
    /// Manage the settings for Strava.
    Strava {
        #[clap(subcommand)]
        command: strava::Command,
    },
}

impl Exec for PlatformCommand {
    fn exec(&self, global: &Global) -> Result<()> {
        match self {
            #[cfg(feature = "strava")]
            Self::Strava { command } => command.inner().exec(global),
            #[cfg(not(feature = "strava"))]
            _ => {
                let _ = global;
                Ok(())
            }
        }
    }
}

#[derive(Clone, Copy, Debug, ValueEnum)]
pub enum PlatformSpec {
    #[cfg(feature = "intervals_icu")]
    #[clap(name = "intervals.icu")]
    IntervalsIcu,
    #[cfg(feature = "strava")]
    Strava,
}
