// Copyright (C) Robin Krahl <robin.krahl@ireas.org>
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{path::PathBuf, thread, time::Duration};

use anyhow::Result;
use clap::{Args, Subcommand};
use indicatif::{MultiProgress, ProgressBar};

use crate::platforms::{Platform, PlatformArgs, PlatformCommand, PlatformSpec};

#[derive(Debug)]
pub struct Global {
    pub platform: PlatformArgs,
}

pub trait Exec {
    fn exec(&self, global: &Global) -> Result<()>;
}

#[derive(Debug, Subcommand)]
pub enum Command {
    Upload(Upload),
    #[clap(flatten)]
    Platform(PlatformCommand),
}

impl Command {
    pub fn inner(&self) -> &dyn Exec {
        match self {
            Self::Upload(upload) => upload,
            Self::Platform(platform) => platform,
        }
    }
}

impl Exec for Command {
    fn exec(&self, global: &Global) -> Result<()> {
        self.inner().exec(global)
    }
}

/// Upload an activity.
#[derive(Debug, Args)]
pub struct Upload {
    /// The name of the activity.
    #[clap(long, short)]
    name: Option<String>,

    /// The platforms to upload the activity to.
    #[clap(long, short)]
    platforms: Vec<PlatformSpec>,

    /// The path of the activity file.
    path: PathBuf,
}

impl Upload {
    fn upload_to_platform(&self, platform: &dyn Platform) -> (String, bool) {
        let result = platform.upload(&self.path, self.name.as_deref());
        let message = match &result {
            Ok(id) => {
                format!("upload finished (ID {id})")
            }
            Err(err) => {
                format!("upload failed: {err:?}")
            }
        };
        (message, result.is_ok())
    }
}

impl Exec for Upload {
    fn exec(&self, global: &Global) -> Result<()> {
        let platforms = global.platform.platforms(&self.platforms);
        let max_id_len = platforms
            .iter()
            .map(|platform| platform.id().len())
            .max()
            .unwrap_or_default();
        let mp = MultiProgress::new();
        let failed_platforms: Vec<_> = thread::scope(|s| {
            let mut threads = Vec::new();
            for platform in &platforms {
                let p = ProgressBar::new_spinner()
                    .with_message(format!("[{:max_id_len$}] uploading", platform.id()));
                let p = mp.add(p);
                p.enable_steady_tick(Duration::from_millis(100));
                threads.push(s.spawn(move || {
                    let (message, success) = self.upload_to_platform(*platform);
                    p.finish_with_message(format!("[{:max_id_len$}] {message}", platform.id()));
                    success
                }));
            }

            let mut failed_platforms = Vec::new();
            for (thread, platform) in threads.into_iter().zip(&platforms) {
                let success = thread.join().unwrap_or_else(|err| {
                    eprintln!("Upload for {} panicked: {err:?}", platform.id());
                    false
                });
                if !success {
                    failed_platforms.push(platform.id());
                }
            }
            failed_platforms
        });
        if failed_platforms.is_empty() {
            Ok(())
        } else {
            Err(anyhow::anyhow!(
                "Failed to upload to {} platforms: {}",
                failed_platforms.len(),
                failed_platforms.join(", ")
            ))
        }
    }
}
