// Copyright (C) Robin Krahl <robin.krahl@ireas.org>
// SPDX-License-Identifier: GPL-3.0-or-later

#![warn(
    non_ascii_idents,
    trivial_casts,
    unused,
    unused_qualifications,
    clippy::expect_used,
    clippy::unwrap_used,
    clippy::panic
)]
#![deny(unsafe_code)]

mod args;
mod commands;
mod config;
mod platforms;
mod utils;

use std::process::ExitCode;

use anyhow::Result;
use merge::Merge as _;

use crate::commands::{Exec as _, Global};

fn main() -> ExitCode {
    env_logger::init();
    match exec() {
        Ok(()) => ExitCode::SUCCESS,
        Err(err) => {
            eprintln!("Error: {:#}", err);
            ExitCode::FAILURE
        }
    }
}

fn exec() -> Result<()> {
    let args = args::parse();
    let config = config::load(args.config.as_deref())?;

    let mut platform = args.platform;
    platform.merge(config.platform);

    let global = Global { platform };
    args.command.exec(&global)
}
