// Copyright (C) Robin Krahl <robin.krahl@ireas.org>
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{fs, io, path::Path, thread, time::Duration};

use anyhow::{Context as _, Result};
use clap::{Args, Subcommand};
use merge::Merge;
use secrecy::SecretString;
use serde::Deserialize;
use trackme_backends::strava::{Api, Config, State, Upload, UploadOptions};
use url::Url;

use crate::{
    commands::{Exec, Global},
    platforms::Platform,
    utils,
};

#[derive(Debug, Default, Args, Deserialize, Merge)]
#[clap(next_help_heading = "Strava Options")]
pub struct Strava {
    /// The client ID for the Strava API.
    #[clap(long = "strava-client-id", global = true)]
    client_id: Option<i64>,

    /// The client secret for the Strava API.
    #[clap(long = "strava-client-secret", global = true)]
    client_secret: Option<SecretString>,
}

impl Strava {
    fn config(&self) -> Result<Config<'_>> {
        let client_id = self.client_id.context("missing client ID for Strava")?;
        let client_secret = self
            .client_secret
            .as_ref()
            .context("missing client secret for Strava")?;
        Ok(Config {
            client_id,
            client_secret,
        })
    }

    fn api(&self) -> Result<Api> {
        let agent = utils::agent();
        let config = self.config()?;
        let mut state = load_state()?;
        let result = Api::authorize(agent, &config, &mut state);
        save_state(&state)?;
        result.context("failed to authorize with the Strava API")
    }
}

impl Platform for Strava {
    fn id(&self) -> &'static str {
        "strava"
    }

    fn upload(&self, path: &Path, name: Option<&str>) -> Result<String> {
        let api = self.api()?;
        let options = UploadOptions {
            name,
            ..Default::default()
        };
        let mut upload = api
            .upload(path, options)
            .context("failed to upload activity to Strava")?;
        loop {
            if let Upload::Finished { activity_id } = upload {
                return Ok(activity_id.to_string());
            }
            thread::sleep(Duration::from_secs(1));
            upload = upload.update(&api)?;
        }
    }
}

#[derive(Debug, Subcommand)]
pub enum Command {
    /// Login to Strava and request an access token.
    Login(Login),
    /// Logout from Strava and revoke the access token.
    Logout(Logout),
}

impl Command {
    pub fn inner(&self) -> &dyn Exec {
        match self {
            Self::Login(login) => login,
            Self::Logout(logout) => logout,
        }
    }
}

#[derive(Debug, Args)]
pub struct Login {
    /// Open the login page in the system web browser.
    #[clap(long, short)]
    pub open: bool,

    /// The redirect URL for the authorization request.
    #[clap(long, short, default_value = "http://localhost/exchange_token")]
    pub redirect: String,
}

impl Exec for Login {
    fn exec(&self, global: &Global) -> Result<()> {
        let config = global.platform.strava.config()?;
        let url = config
            .login_url(&self.redirect)
            .context("failed to construct login URL")?;
        if self.open {
            if let Err(err) = open::that(url.as_str()) {
                eprintln!("Failed to open web browser: {err}");
            }
        }
        println!(
            "Please open this URL in your web browser, authorize the application and \
            paste the URL you are redirected to below:"
        );
        println!("\t{url}");

        let mut url = String::new();
        io::stdin()
            .read_line(&mut url)
            .context("failed to read from stdin")?;
        let url = Url::parse(&url).context("failed to parse redirect URL")?;
        let code = url
            .query_pairs()
            .find(|(key, _)| key == "code")
            .map(|(_, value)| value)
            .context("missing code in redirect URL")?;
        let code = SecretString::new(code.into_owned());

        let agent = utils::agent();
        let (_, state) =
            Api::login(agent, &config, &code).context("failed to login to the Strava API")?;
        save_state(&state)?;

        Ok(())
    }
}

#[derive(Debug, Args)]
pub struct Logout;

impl Exec for Logout {
    fn exec(&self, global: &Global) -> Result<()> {
        let api = global.platform.strava.api()?;
        api.deauthorize().map_err(|(err, _)| err)?;
        delete_state()
    }
}

const STATE_FILE: &str = "strava.toml";

fn save_state(state: &State) -> Result<()> {
    let file = utils::base_directories()
        .context("failed to determine XDG base directories")?
        .place_state_file(STATE_FILE)
        .context("failed to create state directory")?;
    let s = toml::to_string(state).context("failed to serialize state")?;
    fs::write(&file, s).with_context(|| format!("failed to write state file {}", file.display()))
}

fn load_state() -> Result<State> {
    let file = utils::base_directories()
        .context("failed to determine XDG base directories")?
        .find_state_file(STATE_FILE)
        .context("failed to find state file – use the `strava login` command to create it")?;
    let s = fs::read_to_string(&file)
        .with_context(|| format!("failed to read state file {}", file.display()))?;
    toml::from_str(&s).with_context(|| format!("failed to parse state file {}", file.display()))
}

fn delete_state() -> Result<()> {
    let file = utils::base_directories()
        .context("failed to determine XDG base directories")?
        .find_state_file(STATE_FILE);
    if let Some(file) = file {
        fs::remove_file(&file)
            .with_context(|| format!("failed to delete state file {}", file.display()))?;
    }
    Ok(())
}
