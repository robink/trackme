// Copyright (C) Robin Krahl <robin.krahl@ireas.org>
// SPDX-License-Identifier: GPL-3.0-or-later

use std::path::Path;

use anyhow::{Context as _, Result};
use clap::Args;
use merge::Merge;
use secrecy::SecretString;
use serde::Deserialize;
use trackme_backends::intervals_icu::Api;

use crate::{platforms::Platform, utils};

#[derive(Debug, Default, Args, Deserialize, Merge)]
#[clap(next_help_heading = "Intervals.icu Options")]
pub struct IntervalsIcu {
    /// The athelete ID on Intervals.icu.
    #[clap(long = "intervals-icu-athlete-id", global = true)]
    athlete_id: Option<String>,

    /// The API key for Intervals.icu.
    #[clap(long = "intervals-icu-api-key", global = true)]
    api_key: Option<SecretString>,
}

impl IntervalsIcu {
    fn api(&self) -> Result<Api<'_>> {
        let agent = utils::agent();
        let athlete_id = self
            .athlete_id
            .as_deref()
            .context("missing athlete ID for Intervals.icu")?;
        let api_key = self
            .api_key
            .as_ref()
            .context("missing API key for Intervals.icu")?;
        Ok(Api::new(agent, athlete_id, api_key))
    }
}

impl Platform for IntervalsIcu {
    fn id(&self) -> &'static str {
        "intervals.icu"
    }

    fn upload(&self, path: &Path, name: Option<&str>) -> Result<String> {
        self.api()?
            .upload(path, name)
            .context("failed to upload activity to Intervals.icu")
    }
}
