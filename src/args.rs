// Copyright (C) Robin Krahl <robin.krahl@ireas.org>
// SPDX-License-Identifier: GPL-3.0-or-later

use std::path::PathBuf;

use clap::Parser;

use crate::{commands::Command, platforms::PlatformArgs};

pub fn parse() -> Args {
    Parser::parse()
}

/// Upload activity logs to analysis platforms.
#[derive(Debug, Parser)]
#[clap(author, version)]
pub struct Args {
    /// The configuration file
    #[clap(short, long)]
    pub config: Option<PathBuf>,

    #[clap(flatten)]
    pub platform: PlatformArgs,

    #[clap(subcommand)]
    pub command: Command,
}
