// Copyright (C) Robin Krahl <robin.krahl@ireas.org>
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{
    borrow::Cow,
    fs,
    path::{Path, PathBuf},
};

use anyhow::{Context as _, Result};
use serde::Deserialize;

use crate::{platforms::PlatformArgs, utils};

pub fn load(path: Option<&Path>) -> Result<Config> {
    let path = path
        .map(Cow::Borrowed)
        .or_else(|| find_config_file().map(Cow::Owned));
    if let Some(path) = path.as_deref() {
        Config::load(path)
    } else {
        Ok(Default::default())
    }
}

fn find_config_file() -> Option<PathBuf> {
    utils::base_directories()?.find_config_file("config.toml")
}

#[derive(Debug, Default, Deserialize)]
pub struct Config {
    #[serde(default)]
    pub platform: PlatformArgs,
}

impl Config {
    fn load(path: &Path) -> Result<Self> {
        let config = fs::read_to_string(path)
            .with_context(|| format!("failed to read config file {}", path.display()))?;
        toml::from_str(&config)
            .with_context(|| format!("failed to parse config file {}", path.display()))
    }
}
