<!---
Copyright (C) Robin Krahl <robin.krahl@ireas.org>
SPDX-License-Identifier: CC0-1.0
-->

# Changelog

## `trackme`

### v0.1.0 (2023-10-22)

Initial CLI release with support for uploading activities to Intervals.icu and
Strava.

## `trackme-backends`

### v0.1.0 (2023-10-22)

Initial library release with support for uploading activities to Intervals.icu
and Strava.

