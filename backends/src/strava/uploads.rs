// Copyright (C) Robin Krahl <robin.krahl@ireas.org>
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{
    fmt::{self, Display, Formatter},
    fs::File,
    path::Path,
};

use multipart::client::lazy::Multipart;
use serde::Deserialize;

use super::context::{AccessToken, Context};
use crate::{Error, ErrorKind, Result};

/// The options for a file upload.
#[derive(Clone, Copy, Debug, Default)]
pub struct UploadOptions<'a> {
    /// The data type of the file.
    ///
    /// If not set, it is automatically determined from the file extension.
    pub data_type: Option<DataType>,

    /// The name of the activity.
    ///
    /// If not set, it is set automatically by Strava.
    pub name: Option<&'a str>,

    /// The description of the activity.
    pub description: Option<&'a str>,

    /// The sport type of the activity.
    ///
    /// If not set, it is automatically determined by Strava.
    pub sport_type: Option<&'a str>,
}

#[derive(Clone, Copy, Debug)]
pub enum DataType {
    Fit,
    FitGz,
    Gpx,
    GpxGz,
    Tcx,
    TcxGz,
}

impl DataType {
    fn new(extension: &str, zipped: bool) -> Option<Self> {
        let data_type = match (extension.to_ascii_lowercase().as_str(), zipped) {
            ("fit", false) => Self::Fit,
            ("fit", true) => Self::FitGz,
            ("tcx", false) => Self::Tcx,
            ("tcx", true) => Self::TcxGz,
            ("gpx", false) => Self::Gpx,
            ("gpx", true) => Self::GpxGz,
            _ => {
                return None;
            }
        };
        Some(data_type)
    }

    pub fn guess(path: &Path) -> Option<Self> {
        fn ext(path: &Path) -> Option<&str> {
            path.extension()?.to_str()
        }

        let mut extension = ext(path)?;
        let zipped = extension.eq_ignore_ascii_case("gz");
        if zipped {
            extension = ext(path.file_stem()?.as_ref())?;
        }
        Self::new(extension, zipped)
    }

    pub fn as_str(&self) -> &'static str {
        match self {
            Self::Fit => "fit",
            Self::FitGz => "fit.gz",
            Self::Tcx => "tcx",
            Self::TcxGz => "tcx.gz",
            Self::Gpx => "gpx",
            Self::GpxGz => "gpx.gz",
        }
    }
}

impl Display for DataType {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        self.as_str().fmt(f)
    }
}

#[derive(Debug, Deserialize)]
pub struct UploadResponse {
    pub id: Option<i64>,
    pub id_str: Option<String>,
    pub external_id: Option<String>,
    pub error: Option<String>,
    pub status: Option<String>,
    pub activity_id: Option<i64>,
}

pub fn create(
    context: &Context<AccessToken>,
    path: &Path,
    options: UploadOptions<'_>,
) -> Result<UploadResponse> {
    let mut multipart = Multipart::new();
    let file = File::open(path).map_err(|err| {
        Error::new(
            ErrorKind::FileReadingFailed {
                path: path.to_owned(),
            },
            err,
        )
    })?;
    multipart.add_stream(
        "file",
        file,
        path.file_name().and_then(|s| s.to_str()),
        None,
    );
    let data_type = options
        .data_type
        .or_else(|| DataType::guess(path))
        .ok_or_else(|| ErrorKind::DataTypeParsingFailed {
            filename: path.to_owned(),
        })?;
    multipart.add_text("data_type", data_type.as_str());
    let options = [
        ("name", &options.name),
        ("description", &options.description),
        ("sport_type", &options.sport_type),
    ];
    for (name, value) in options {
        if let Some(value) = value {
            multipart.add_text(name, *value);
        }
    }
    context
        .post_form("uploads", &mut multipart)?
        .into_json()
        .map_err(|err| Error::new(ErrorKind::ResponseFailed, err))
}

pub fn get(context: &Context<AccessToken>, id: i64) -> Result<UploadResponse> {
    context
        .get(&format!("uploads/{}", id))?
        .into_json()
        .map_err(|err| Error::new(ErrorKind::ResponseFailed, err))
}
