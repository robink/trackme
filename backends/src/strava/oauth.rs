// Copyright (C) Robin Krahl <robin.krahl@ireas.org>
// SPDX-License-Identifier: GPL-3.0-or-later

use serde::Deserialize;

use super::context::{Authorization, Context};
use crate::{Error, ErrorKind, Result};

#[derive(Debug, Deserialize)]
pub struct Token {
    pub token_type: String,
    pub access_token: String,
    pub expires_at: i64,
    pub expires_in: i64,
    pub refresh_token: String,
}

pub fn request<A: Authorization>(
    context: &Context<A>,
    client_id: i64,
    client_secret: &str,
    code: &str,
) -> Result<Token> {
    context
        .request("POST", "/oauth/token")?
        .query("client_id", &client_id.to_string())
        .query("client_secret", client_secret)
        .query("grant_type", "authorization_code")
        .query("code", code)
        .call()?
        .into_json()
        .map_err(|err| Error::new(ErrorKind::ResponseFailed, err))
}

pub fn refresh<A: Authorization>(
    context: &Context<A>,
    client_id: i64,
    client_secret: &str,
    refresh_token: &str,
) -> Result<Token> {
    context
        .request("POST", "/oauth/token")?
        .query("client_id", &client_id.to_string())
        .query("client_secret", client_secret)
        .query("grant_type", "refresh_token")
        .query("refresh_token", refresh_token)
        .call()?
        .into_json()
        .map_err(|err| Error::new(ErrorKind::ResponseFailed, err))
}

pub fn deauthorize<A: Authorization>(context: &Context<A>, access_token: &str) -> Result<()> {
    context
        .request("POST", "/oauth/deauthorize")?
        .query("access_token", access_token)
        .call()?;
    Ok(())
}
