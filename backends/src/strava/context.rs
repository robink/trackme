// Copyright (C) Robin Krahl <robin.krahl@ireas.org>
// SPDX-License-Identifier: GPL-3.0-or-later

use multipart::client::lazy::Multipart;
use ureq::{Agent, Request, Response};
use url::Url;

use crate::{Error, ErrorKind, Result};

const BASE_URL: &str = "https://www.strava.com/api/v3/";

#[derive(Debug)]
pub struct Context<A: Authorization> {
    agent: Agent,
    base_url: Url,
    authorization: A,
}

impl Context<NoAuthorization> {
    pub fn new(agent: Agent) -> Result<Self> {
        let base_url = Url::parse(BASE_URL).map_err(|err| {
            Error::new(
                ErrorKind::UrlParsingFailed {
                    url: BASE_URL.to_owned(),
                },
                err,
            )
        })?;
        Ok(Self {
            agent,
            base_url,
            authorization: NoAuthorization,
        })
    }
}

impl<A: Authorization> Context<A> {
    pub fn with_access_token(self, access_token: &str) -> Context<AccessToken> {
        Context {
            agent: self.agent,
            base_url: self.base_url,
            authorization: AccessToken::new(access_token),
        }
    }

    pub fn authorization(&self) -> &A {
        &self.authorization
    }

    pub fn endpoint(&self, endpoint: &str) -> Result<Url> {
        self.base_url.join(endpoint).map_err(|err| {
            Error::new(
                ErrorKind::UrlParsingFailed {
                    url: endpoint.to_owned(),
                },
                err,
            )
        })
    }

    pub fn request(&self, method: &str, endpoint: &str) -> Result<Request> {
        let mut request = self.agent.request_url(method, &self.endpoint(endpoint)?);
        if let Some(authorization) = self.authorization.authorization() {
            request = request.set("Authorization", authorization);
        }
        Ok(request)
    }

    pub fn get(&self, endpoint: &str) -> Result<Response> {
        self.request("GET", endpoint)?.call().map_err(From::from)
    }

    pub fn post_form(&self, endpoint: &str, multipart: &mut Multipart<'_, '_>) -> Result<Response> {
        let data = multipart.prepare()?;
        let mut request = self.request("POST", endpoint)?.set(
            "Content-Type",
            &format!("multipart/form-data; boundary={}", data.boundary()),
        );
        if let Some(content_len) = data.content_len() {
            request = request.set("Content-Length", &content_len.to_string());
        }
        request.send(data).map_err(From::from)
    }
}

pub trait Authorization {
    fn authorization(&self) -> Option<&str> {
        None
    }
}

const BEARER: &str = "Bearer ";

#[derive(Clone, Debug)]
pub struct AccessToken(String);

impl AccessToken {
    fn new(token: &str) -> Self {
        Self(format!("{BEARER}{token}"))
    }

    pub fn token(&self) -> &str {
        &self.0[BEARER.len()..]
    }
}

impl Authorization for AccessToken {
    fn authorization(&self) -> Option<&str> {
        Some(&self.0)
    }
}

#[derive(Clone, Copy, Debug)]
pub struct NoAuthorization;

impl Authorization for NoAuthorization {}
