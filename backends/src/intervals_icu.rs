// Copyright (C) Robin Krahl <robin.krahl@ireas.org>
// SPDX-License-Identifier: GPL-3.0-or-later

//! Provides access to [Intervals.icu][].
//!
//! [Intervals.icu]: https://intervals.icu

use std::{fs::File, path::Path};

use data_encoding::BASE64;
use multipart::client::lazy::Multipart;
use secrecy::{ExposeSecret as _, SecretString};
use serde::Deserialize;
use ureq::{Agent, Request, Response};

use crate::{Error, ErrorKind, Result};

/// Provides access to [Intervals.icu][].
///
/// [Intervals.icu]: https://intervals.icu
#[derive(Clone, Debug)]
pub struct Api<'a> {
    agent: Agent,
    athlete_id: &'a str,
    api_key: &'a SecretString,
}

impl<'a> Api<'a> {
    /// Creates a new API instance using the given settings.
    pub fn new(agent: Agent, athlete_id: &'a str, api_key: &'a SecretString) -> Self {
        Self {
            agent,
            athlete_id,
            api_key,
        }
    }

    fn authorization(&self) -> String {
        let credentials = format!("API_KEY:{}", self.api_key.expose_secret());
        let credentials = BASE64.encode(credentials.as_bytes());
        format!("Basic {credentials}")
    }

    /// Uploads the given file with the given name and returns the activity ID.
    pub fn upload(&self, path: &Path, name: Option<&str>) -> Result<String> {
        let url = format!(
            "https://intervals.icu/api/v1/athlete/{}/activities",
            self.athlete_id
        );
        let mut multipart = Multipart::new();
        let file = File::open(path).map_err(|err| {
            Error::new(
                ErrorKind::FileReadingFailed {
                    path: path.to_owned(),
                },
                err,
            )
        })?;
        multipart.add_stream(
            "file",
            file,
            path.file_name().and_then(|s| s.to_str()),
            None,
        );
        let mut request = self
            .agent
            .post(&url)
            .set("Authorization", &self.authorization());
        if let Some(name) = name {
            request = request.query("name", name);
        }
        send_multipart(request, multipart)?
            .into_json::<UploadResponse>()
            .map(|response| response.id)
            .map_err(|err| Error::new(ErrorKind::ResponseFailed, err))
    }
}

#[derive(Debug, Deserialize)]
struct UploadResponse {
    id: String,
}

fn send_multipart(request: Request, mut multipart: Multipart<'_, '_>) -> Result<Response> {
    let data = multipart.prepare()?;
    let mut request = request.set(
        "Content-Type",
        &format!("multipart/form-data; boundary={}", data.boundary()),
    );
    if let Some(content_len) = data.content_len() {
        request = request.set("Content-Length", &content_len.to_string());
    }
    request.send(data).map_err(From::from)
}
