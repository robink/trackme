// Copyright (C) Robin Krahl <robin.krahl@ireas.org>
// SPDX-License-Identifier: GPL-3.0-or-later

//! Provides access to [Strava][].
//!
//! Accessing the Strava API requires client and user authentication.  For the client
//! authentication, [create an API application][] and provide the client ID and secret in
//! [`Config`][].  For the user configuration, point the user to the URL returned by
//! [`Config::login_url`][].  After logging in to Strava and authorizing the application, the user
//! is redirected.  Extract the authorization token from the redirect URL and pass it to
//! [`Api::login`][].  This will request an access token and store it in a [`State`][] instances.
//! Afterwards, use [`Api::authorize`][] to use or refresh the obtained access token.
//!
//! As the access token expires and has to be renewed regularly, make sure that you save the
//! [`State`][] after using the API as the access token may have changed.
//!
//! [Strava]: https://strava.com
//! [create an API application]: https://www.strava.com/settings/api

mod context;
mod oauth;
mod uploads;

use std::path::Path;

use chrono::{DateTime, Duration, Local};
use secrecy::{ExposeSecret as _, SecretString};
use serde::{Deserialize, Serialize};
use ureq::Agent;
use url::Url;

use crate::{Error, ErrorKind, Result};
use context::{AccessToken, Authorization, Context};
use uploads::UploadResponse;

pub use uploads::UploadOptions;

/// The configuration for the Strava API client.
#[derive(Clone, Debug)]
pub struct Config<'a> {
    /// The client ID, assigned by Strava in *Settings* → *My API Application*.
    pub client_id: i64,
    /// The client secret, assigned by Strava in *Settings* → *My API Application*.
    pub client_secret: &'a SecretString,
}

impl Config<'_> {
    /// Returns the URL that can be used to obtain an authorization token.
    ///
    /// Users have to open the URL in their web browser, login to Strava and authorize the
    /// application.  They are then redirected to the given URL, e. g.
    /// `http://localhost/exchange_token`, and have to extract the authroization token from the
    /// `code` query field of the redirected URL.  This token can then be used for the initial API
    /// login with [`Api::login`][].
    pub fn login_url(&self, redirect: &str) -> Result<Url> {
        let url = "https://strava.com/oauth/authorize";
        let mut url = Url::parse(url).map_err(|err| {
            Error::new(
                ErrorKind::UrlParsingFailed {
                    url: url.to_owned(),
                },
                err,
            )
        })?;
        {
            let mut query_pairs = url.query_pairs_mut();
            query_pairs.append_pair("client_id", &self.client_id.to_string());
            query_pairs.append_pair("response_type", "code");
            query_pairs.append_pair("redirect_uri", redirect);
            query_pairs.append_pair("approval_prompt", "force");
            query_pairs.append_pair("scope", "activity:write");
        }
        Ok(url)
    }
}

/// Provides access to [Strava][].
///
/// API operations require authorization.  See [`login`][`Api::login`] and
/// [`authorize`][`Api::authorize`] for more information.  The authorization state is stored in the
/// [`State`][] struct.  Users of this API client must persist this state between invocations so
/// that authorization works, for example by saving it on the filesystem.  The `State` is unique
/// for a user, so multi-user applications should have one `State` instance per user.
///
/// The access tokens used for authorization expire.  Currently, the expiration is only checked
/// in the authorization methods.  Operations may fail if the access token expires afterwards.  In
/// this case, authorization has to be repeated.
///
/// [Strava]: https://strava.com
#[derive(Debug)]
pub struct Api {
    context: Context<AccessToken>,
}

impl Api {
    /// Logs in to the Strava API using the given authorization code.
    ///
    /// This operation only has to be performed once per user.  The authorization code can be
    /// retrieved from the Strava web interface, see [`Config::login_url`][].  This method uses it
    /// to request an access token that can be used for future operations.
    pub fn login(agent: Agent, config: &Config<'_>, code: &SecretString) -> Result<(Api, State)> {
        let context = Context::new(agent)?;
        let token = oauth::request(
            &context,
            config.client_id,
            config.client_secret.expose_secret(),
            code.expose_secret(),
        )?;
        let state = State {
            refresh_token: token.refresh_token,
            access_token: token.access_token,
            expiration: Local::now() + Duration::seconds(token.expires_in),
        };
        let context = context.with_access_token(&state.access_token);
        Ok((Self { context }, state))
    }

    /// Authorizes this `Api` instance, either by using the cached access token from `State` or by
    /// requesting a new access token if that token expired.
    ///
    /// Use [`login`][`Api::login`] for the initial authorization.  The caller must persist the
    /// state between invocations of this method.
    pub fn authorize(agent: Agent, config: &Config, state: &mut State) -> Result<Api> {
        let context = Context::new(agent)?;
        let access_token = state.access_token(&context, config)?;
        let context = context.with_access_token(access_token);
        Ok(Self { context })
    }

    /// Revokes the access token used for this `Api` instance.
    ///
    /// Callers of this method must make sure that the application’s [`State`][] is cleared if this
    /// command is successful.
    // We want to be able to return Self to make it possible to repeat the deauthorization if it
    // fails.
    #[allow(clippy::result_large_err)]
    pub fn deauthorize(self) -> Result<(), (Error, Self)> {
        oauth::deauthorize(&self.context, self.context.authorization().token())
            .map_err(|err| (err, self))
    }

    /// Uploads the file at the given path using the given options.
    ///
    /// The returned [`Upload`][] instance can be used to query the progress of the upload until it
    /// is finished.
    pub fn upload(&self, path: &Path, options: UploadOptions<'_>) -> Result<Upload> {
        uploads::create(&self.context, path, options)?.try_into()
    }
}

/// The status of an upload.
#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub enum Upload {
    /// The upload is still pending.
    Pending {
        /// The upload ID.
        upload_id: i64,
    },
    /// The upload is finished.
    Finished {
        /// The ID of the activity that was created from this upload.
        activity_id: i64,
    },
}

impl Upload {
    /// Updates the status of this upload.
    ///
    /// If the upload is still pending, this function requests an update from the Strava API.  If
    /// the upload is finished, this is a no-op.
    ///
    /// A Strava upload typically takes several seconds, so calling this function once per second
    /// until the upload is finished is a reasonable default.
    pub fn update(&self, api: &Api) -> Result<Self> {
        if let Self::Pending { upload_id } = self {
            uploads::get(&api.context, *upload_id)?.try_into()
        } else {
            Ok(*self)
        }
    }
}

impl TryFrom<UploadResponse> for Upload {
    type Error = Error;

    fn try_from(response: UploadResponse) -> Result<Self> {
        if let Some(error) = response.error {
            Err(Error::new(ErrorKind::RequestFailed, error))
        } else if let Some(activity_id) = response.activity_id {
            Ok(Self::Finished { activity_id })
        } else if let Some(upload_id) = response.id {
            Ok(Self::Pending { upload_id })
        } else {
            Err(Error::new(
                ErrorKind::ResponseFailed,
                "missing upload ID in response",
            ))
        }
    }
}

/// The state of the Strava API client.
///
/// Callers should persist this state between API invocations to make sure that authorized access
/// is working properly.  The state is unique for a user, so multi-user applications should
/// maintain one instance per user.
#[derive(Debug, Deserialize, Serialize)]
pub struct State {
    /// The last access token received from the Strava API.
    pub access_token: String,
    /// The refresh token that was received with the stored access token.
    pub refresh_token: String,
    /// The expiration time of the stored access token.
    pub expiration: DateTime<Local>,
}

impl State {
    fn access_token<A: Authorization>(
        &mut self,
        context: &Context<A>,
        config: &Config,
    ) -> Result<&str> {
        if Local::now() >= self.expiration {
            log::info!(
                "Access token expired on {}, performing refresh",
                self.expiration
            );
            self.refresh(context, config)?;
        }
        Ok(&self.access_token)
    }

    fn refresh<A: Authorization>(&mut self, context: &Context<A>, config: &Config) -> Result<()> {
        let token = oauth::refresh(
            context,
            config.client_id,
            config.client_secret.expose_secret(),
            &self.refresh_token,
        )?;
        log::debug!("Received refresh response: {:?}", token);
        self.refresh_token = token.refresh_token;
        self.access_token = token.access_token;
        self.expiration = Local::now() + Duration::seconds(token.expires_in);
        Ok(())
    }
}
