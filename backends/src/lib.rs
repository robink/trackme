// Copyright (C) Robin Krahl <robin.krahl@ireas.org>
// SPDX-License-Identifier: GPL-3.0-or-later

#![warn(
    missing_copy_implementations,
    missing_debug_implementations,
    missing_docs,
    non_ascii_idents,
    trivial_casts,
    unused,
    unused_qualifications,
    clippy::expect_used,
    clippy::unwrap_used,
    clippy::panic
)]
#![deny(unsafe_code)]

//! Upload activities to analysis platforms.

use std::{
    error,
    fmt::{self, Display, Formatter},
    path::PathBuf,
    result,
};

#[cfg(feature = "intervals_icu")]
pub mod intervals_icu;
#[cfg(feature = "strava")]
pub mod strava;

/// Result type for `trackme-backends`.
pub type Result<T, E = Error> = result::Result<T, E>;

/// Error type for `trackme-backends`.
///
/// An error is a combination of an [`ErrorKind`][] describing the type of the error and an
/// optional [`std::error::Error`][] implementation that caused the error.
#[derive(Debug)]
pub struct Error {
    kind: ErrorKind,
    error: Option<Box<dyn error::Error + Send + Sync>>,
}

impl Error {
    /// Creates a new error with the given kind and inner error.
    pub fn new<E: Into<Box<dyn error::Error + Send + Sync>>>(kind: ErrorKind, error: E) -> Self {
        Self {
            kind,
            error: Some(error.into()),
        }
    }

    /// Returns the error kind.
    pub fn kind(&self) -> &ErrorKind {
        &self.kind
    }

    /// Provides access to the inner error (if set).
    pub fn into_inner(self) -> Option<Box<dyn error::Error + Send + Sync>> {
        self.error
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        self.kind.fmt(f)
    }
}

impl From<ErrorKind> for Error {
    fn from(kind: ErrorKind) -> Self {
        Self { kind, error: None }
    }
}

impl error::Error for Error {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        if let Some(error) = &self.error {
            Some(error.as_ref())
        } else {
            None
        }
    }
}

#[cfg(feature = "multipart")]
impl From<multipart::client::lazy::LazyIoError<'_>> for Error {
    fn from(error: multipart::client::lazy::LazyIoError<'_>) -> Self {
        Self::new(
            ErrorKind::MultipartEncodingFailed {
                field: error.field_name.map(|field| field.into_owned()),
            },
            error.error,
        )
    }
}

#[cfg(feature = "ureq")]
impl From<ureq::Error> for Error {
    fn from(error: ureq::Error) -> Self {
        let mut message = error.to_string();
        if let ureq::Error::Status(_, response) = error {
            if let Ok(response) = response.into_string() {
                use std::fmt::Write as _;
                write!(&mut message, ", message: {}", response).ok();
            }
        }
        Error::new(ErrorKind::RequestFailed, message)
    }
}

/// Error kinds for `trackme-backends`.
#[derive(Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub enum ErrorKind {
    /// Failed to parse the data type from a filename.
    DataTypeParsingFailed {
        /// The filename that could not be parsed.
        filename: PathBuf,
    },
    /// Failed to read a file.
    FileReadingFailed {
        /// The path of the file that was read.
        path: PathBuf,
    },
    /// Failed to encode a multipart message.
    MultipartEncodingFailed {
        /// The field of the multipart message that caused the error.
        field: Option<String>,
    },
    /// Failed to perform an API request.
    RequestFailed,
    /// Failed to read an API response.
    ResponseFailed,
    /// Failed to parse a URL.
    UrlParsingFailed {
        /// The URL that could not be parsed.
        url: String,
    },
}

impl Display for ErrorKind {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Self::DataTypeParsingFailed { filename } => {
                write!(
                    f,
                    "failed to parse data type from filename {}",
                    filename.display()
                )
            }
            Self::FileReadingFailed { path } => {
                write!(f, "failed to read file {}", path.display())
            }
            Self::MultipartEncodingFailed { field } => {
                if let Some(field) = field {
                    write!(f, "failed to encode multipart field {field}")
                } else {
                    write!(f, "failed to encode multipart data")
                }
            }
            Self::RequestFailed => write!(f, "failed to send API request"),
            Self::ResponseFailed => write!(f, "failed to read API response"),
            Self::UrlParsingFailed { url } => write!(f, "failed to parse the URL {}", url),
        }
    }
}
